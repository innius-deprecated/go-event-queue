package event_queue_test

import (
	"fmt"
	"testing"

	"bitbucket.org/to-increase/go-event-queue"
	"github.com/stretchr/testify/assert"
	"math/rand"
	"bitbucket.org/to-increase/go-trn/uuid"
	"encoding/json"
)

func initDeqWithQueue(t *testing.T, queue string) event_queue.DomainEventQueue {
	s := stubSession()
	b := event_queue.NewDomainEventQueueBuilder()
	res, err := b.
		WithSession(s).
		AutoCreateQueue(true).
		WithQueueName(queue).
		Build()

	assert.NotNil(t, res)
	assert.Nil(t, err)
	return res
}

func initDeq(t *testing.T) event_queue.DomainEventQueue {
	return initDeqWithQueue(t, "go-event-queue")
}

func TestPublishEvent(t *testing.T) {
	deq := initDeq(t)
	payload := "my string"
	evt, err := event_queue.NewEventString(payload)
	assert.Nil(t, err)
	err = deq.PublishEvent(evt)
	assert.Nil(t, err)
}

func TestNilShouldFail(t *testing.T) {
	deq := initDeq(t)
	err := deq.PublishEvent(nil)
	assert.NotNil(t, err)
}

func TestPublishJson(t *testing.T) {
	deq := initDeq(t)
	type foo struct {
		Foo string `json:"foo"`
		Bar int    `json:"bar"`
	}
	payload := &foo{
		Foo: "foo",
		Bar: 42,
	}
	evt, err := event_queue.NewEventJson(payload)
	assert.Nil(t, err)
	err = deq.PublishEvent(evt)
	assert.Nil(t, err)
}

func TestPublishRawEventJson(t *testing.T) {
	deq := initDeqWithQueue(t, uuid.NewV4().String())
	type foo struct {
		Foo string `json:"foo"`
		Bar int    `json:"bar"`
	}
	payload := &foo{
		Foo: "foo",
		Bar: 42,
	}
	evt, err := event_queue.NewRawEventJson("TestEvent", payload)
	assert.Nil(t, err)
	err = deq.PublishEvent(evt)
	assert.Nil(t, err)
	event, err := deq.ReadEventBlocking(5)
	assert.NoError(t, err)
	rawevent := event_queue.RawEvent{}
	err = json.Unmarshal([]byte(event.Message()), &rawevent)
	assert.Equal(t, "TestEvent", rawevent.Type)
	result := foo{}
	err = json.Unmarshal(rawevent.Data, &result)
	assert.NoError(t, err)
	assert.Equal(t, payload.Foo, result.Foo)
	assert.Equal(t, payload.Bar, result.Bar)
}

func TestPublishEventBatch(t *testing.T) {
	deq := initDeq(t)
	len := 5
	evts := make([]event_queue.DomainEvent, len)
	for i := 0; i < len; i++ {
		evt := event_queue.NewEventStringUnsafe(fmt.Sprintf("Event # %d", i))
		evts[i] = evt
	}

	err := deq.PublishEvents(evts)
	assert.Nil(t, err)
}

func TestPublishEventHugeBatch(t *testing.T) {
	deq := initDeq(t)
	len := 500
	evts := make([]event_queue.DomainEvent, len)
	for i := 0; i < len; i++ {
		evt := event_queue.NewEventStringUnsafe(fmt.Sprintf("Event # %d", i))
		evts[i] = evt
	}

	err := deq.PublishEvents(evts)
	assert.Nil(t, err)
}

func randomString(strlen int) string {
	rand.Seed(12345) // doesn't need te be random, predictable is fine.
	const chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_"
	result := make([]byte, strlen)
	for i := 0; i < strlen; i++ {
		result[i] = chars[rand.Intn(len(chars))]
	}
	return string(result)
}

func TestPublishEventHugeBatchLargeMessage(t *testing.T) {
	deq := initDeq(t)
	len := 15
	fifty_kb := 50000
	evts := make([]event_queue.DomainEvent, len)
	for i := 0; i < len; i++ {
		evt := event_queue.NewEventStringUnsafe(randomString(fifty_kb))
		evts[i] = evt
	}

	err := deq.PublishEvents(evts)
	assert.Nil(t, err)
}

func TestPublishEventToExistingQueue(t *testing.T) {
	qname := "existing_queue"
	initDeqWithQueue(t, qname)

	s := stubSession()
	b := event_queue.NewDomainEventQueueBuilder()
	res, err := b.
		WithSession(s).
		AutoCreateQueue(false).
		WithQueueUrl(fmt.Sprintf("http://localhost:9324/queue/%s", qname)).
		Build()
	assert.NotNil(t, res)
	assert.Nil(t, err)

	res.ReadEventsBlocking(0, 0)

	t.Run("use existing queue by name", func(t *testing.T) {
		b := event_queue.NewDomainEventQueueBuilder()
		res, err := b.
			WithSession(s).
			AutoCreateQueue(false).
			WithQueueName(qname).
			Build()
		assert.NotNil(t, res)
		assert.Nil(t, err)

		res.ReadEventsBlocking(0, 0)
	})
}


