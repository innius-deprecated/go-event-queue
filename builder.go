package event_queue

import (
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/pkg/errors"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/aws"
	)

func NewDomainEventQueueBuilder() DomainEventQueueBuilder {
	return &qb{
		name:       "",
		session:    nil,
		autocreate: false,
		timeout:    minimum_visibility_timeout,
	}
}

type DomainEventQueueBuilder interface {
	// Provide an AWS session
	WithSession(*session.Session) DomainEventQueueBuilder
	// Provide the url of an existing queue
	// Specify either a the queue name or the url
	WithQueueUrl(url string) DomainEventQueueBuilder
	// Provide a name of the queue to use.
	// Specify either a the queue name or the url
	WithQueueName(queue_name string) DomainEventQueueBuilder
	// Should the queue be created if it doesn't exist? Defaults to true.
	AutoCreateQueue(bool) DomainEventQueueBuilder
	// The duration that a message stays hidden after being handed to a consumer.
	// After this time, it can be given to another consumer again. Defaults to 1 minute.
	WithVisibilityTimeout(time.Duration) DomainEventQueueBuilder
	// Build the queue object with the given configuration
	Build() (DomainEventQueue, error)
}

type qb struct {
	session    *session.Session
	name       string
	autocreate bool
	timeout    time.Duration
	queueUrl   string
}

func (q *qb) Build() (DomainEventQueue, error) {
	if q.session == nil {
		return nil, errors.New("A session paramater is mandatory.")
	}

	if q.name == "" {
		if q.autocreate {
			return nil, errors.New("A queue name is mandatory")
		} else if q.queueUrl == "" {
			return nil, errors.New("A queue url is mandatory")
		}
	}

	if q.timeout.Seconds() < minimum_visibility_timeout.Seconds() {
		return nil, errors.New(fmt.Sprintf("The minimum allowed visibility duration is %v, provided with %v", minimum_visibility_timeout, q.timeout))
	}
	if q.timeout.Seconds() > maximum_visibility_timeout.Seconds() {
		return nil, errors.New(fmt.Sprintf("The maximum allowed visibility duration is %v, provided with %v", maximum_visibility_timeout, q.timeout))
	}

	deq := &domain_event_queue{
		session:           q.session,
		queueName:         q.name,
		queueUrl:          q.queueUrl,
		shouldCreateQueue: q.autocreate,
		visibilityTimeout: int64(q.timeout.Seconds()),
	}

	if deq.queueUrl == "" {
		deq.sqs = sqs.New(deq.session)
	} else {
		deq.sqs = sqs.New(deq.session, aws.NewConfig().WithEndpoint(deq.queueUrl))
	}


	// get the queue url from an existing queue
	if !q.autocreate && (q.name != "" && q.queueUrl == "") {
		res, err := deq.sqs.GetQueueUrl(&sqs.GetQueueUrlInput{QueueName: aws.String(deq.queueName)})
		if err != nil {
			return nil, errors.Wrapf(err, "could not get the url of queue %s", deq.queueName)
		}
		deq.queueUrl = aws.StringValue(res.QueueUrl)
	}

	err := deq.initialize()

	if err != nil {
		return nil, errors.Wrap(err, "domain event queue initialization failed.")
	}

	return deq, nil
}

func (q *qb) WithSession(s *session.Session) DomainEventQueueBuilder {
	q.session = s
	return q
}

func (q *qb) WithQueueName(name string) DomainEventQueueBuilder {
	q.name = name
	return q
}

func (q *qb) WithQueueUrl(queueUrl string) DomainEventQueueBuilder {
	q.queueUrl = queueUrl
	return q
}

func (q *qb) AutoCreateQueue(choice bool) DomainEventQueueBuilder {
	q.autocreate = choice
	return q
}

func (q *qb) WithVisibilityTimeout(duration time.Duration) DomainEventQueueBuilder {
	q.timeout = duration
	return q
}
