package event_queue_test

import (
	"bitbucket.org/to-increase/go-event-queue"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEventFromString(t *testing.T) {
	e, err := event_queue.NewEventString("foo bar baz")
	assert.Nil(t, err)
	assert.Equal(t, "foo bar baz", e.Message())
}

func TestJSONFromString(t *testing.T) {
	type m struct {
		Str string `json:"str"`
		Num int    `json:"num"`
	}
	payload := &m{
		Str: "abc",
		Num: 123,
	}
	evt, err := event_queue.NewEventJson(payload)
	assert.Nil(t, err)
	assert.Equal(t, "{\"str\":\"abc\",\"num\":123}", evt.Message())
}
