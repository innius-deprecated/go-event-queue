package event_queue_test

import (
	"fmt"
	"net"
	"testing"

	"bitbucket.org/to-increase/go-event-queue"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/stretchr/testify/assert"
)

// Default elasticmq local address
const host = "localhost"
const port = 9324
const elastic_mq_distrib = "https://s3-eu-west-1.amazonaws.com/softwaremill-public/elasticmq-server-0.9.3.jar"

func stubSession() *session.Session {
	return session.New(
		aws.NewConfig().
			WithRegion("us-east-1").
			WithEndpoint(fmt.Sprintf("http://%v:%v", host, port)),
	)
}

func TestElasticMQRunning(t *testing.T) {
	conn, err := net.Dial("tcp", fmt.Sprintf("%v:%v", host, port))
	if err != nil {
		panic(fmt.Sprintf("Aborting tests. Local SQS (ElasticMQ) is not running. You can get it here: %v", elastic_mq_distrib))
	}
	conn.Close()
}

func TestMandatorySession(t *testing.T) {
	b := event_queue.NewDomainEventQueueBuilder()
	res, err := b.WithQueueName("FooQueue").Build()
	assert.Nil(t, res)
	assert.NotNil(t, err)
}

func TestMandatoryQueueName(t *testing.T) {
	b := event_queue.NewDomainEventQueueBuilder()
	res, err := b.WithSession(stubSession()).Build()
	assert.Nil(t, res)
	assert.NotNil(t, err)
}

func TestValidBuilderInvocation(t *testing.T) {
	b := event_queue.NewDomainEventQueueBuilder()
	res, err := b.
		WithSession(stubSession()).
		AutoCreateQueue(true).
		WithQueueName("MyQueueName").
		Build()

	assert.NotNil(t, res)
	assert.Nil(t, err)
}

func TestWithQueueURL(t *testing.T) {
	b := event_queue.NewDomainEventQueueBuilder()
	res, err := b.
		WithSession(stubSession()).
		AutoCreateQueue(false).
		WithQueueUrl("http://localhost:9324/plantage").
		Build()

	assert.NotNil(t, res)
	assert.Nil(t, err)
}
