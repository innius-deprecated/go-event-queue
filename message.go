package event_queue

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"
	"bitbucket.org/to-increase/go-trn/uuid"
)

func init() {
	rand.Seed(time.Now().Unix())
}

type DomainEvent interface {
	// Retrieve the ID of this event
	ID() string
	// Get the message of this event
	Message() string
	// Unmarshal the message into a pointer
	Unmarshal(interface{}) error
}

type AckableDomainEvent interface {
	DomainEvent
	// Confirm that you are done processing this message.
	Acknowledge() error
	// Extend the visibility of the current message
	ExtendVisibility(duration time.Duration) error
	// Return the original sent timestamp of this message
	SentTimestamp() time.Time
	// Return the approximate delivery count of this message
	DeliveryCount() int
	// Retry the message again with a backoff strategy
	DelayedRetry() error

	Receipt() string
}

type event struct {
	id      string
	message string
	// Only used by ackable domain event (for confirmation)
	receipt  string
	sent_ts  time.Time
	recv_cnt int
	cleanup  func() error
	extend   func(time.Duration) error
}

// Create a new event from a given payload string
func NewEventString(payload string) (DomainEvent, error) {
	if len(payload) > max_message_size {
		return nil, errors.New(fmt.Sprintf("Message size is too big, can only be %v", max_message_size))
	}
	return &event{
		id:      uuid.NewV4().String(),
		message: payload,
	}, nil
}

// Create a new event from a given payload string
// This method is unsafe, and won't throw an error.
// The result might be null.
func NewEventStringUnsafe(payload string) DomainEvent {
	e, err := NewEventString(payload)
	if err != nil {
		return nil
	}
	return e
}

// Create a new event from a given interface, by JSON-encoding it.
func NewEventJson(payload interface{}) (DomainEvent, error) {
	b, e := json.Marshal(payload)
	if e != nil {
		return nil, errors.Wrap(e, "Json marshalling failed.")
	}
	return NewEventString(string(b))
}

// Create a new event adhering to raw event format from a given interface and a type string.
func NewRawEventJson(typ string, payload interface{}) (DomainEvent, error) {
	b1, e := json.Marshal(payload)
	if e != nil {
		return nil, errors.Wrap(e, "Json marshalling failed.")
	}
	b2, e := json.Marshal(RawEvent{Type: typ, Data: b1})
	if e != nil {
		return nil, errors.Wrap(e, "Json marshalling of raw event failed.")
	}
	return NewEventString(string(b2))
}

// Create a new event from a given interface, by JSON-encoding it.
// This method is unsafe, and won't throw an error.
// The result might be null.
func NewEventJsonUnsafe(payload interface{}) DomainEvent {
	e, err := NewEventJson(payload)
	if err != nil {
		return nil
	}
	return e
}

// in: ms since epoch as string, out ts or epoch
func parseTimestamp(raw string) time.Time {
	i, err := strconv.ParseInt(raw, 10, 64)
	if err != nil {
		return time.Unix(0, 0)
	}
	return time.Unix(i, 0)
}

func newEvent(q DomainEventQueue, m *sqs.Message) AckableDomainEvent {
	evt := &event{
		id:       *m.MessageId,
		message:  *m.Body,
		receipt:  *m.ReceiptHandle,
		sent_ts:  parseTimestamp(*m.Attributes["SentTimestamp"]),
		recv_cnt: -1,
	}
	i, err := strconv.ParseInt(*m.Attributes["ApproximateReceiveCount"], 10, 32)
	if err == nil {
		evt.recv_cnt = int(i)
	}
	evt.cleanup = func() error {
		return q.DeleteEvent(evt)
	}
	evt.extend = func(duration time.Duration) error {
		return q.ExtendVisibilityTimeout(evt, duration)
	}
	return evt
}

func (e *event) ID() string {
	return e.id
}

func (e *event) Message() string {
	return e.message
}

func (e *event) Receipt() string {
	return e.receipt
}

func (e *event) Unmarshal(v interface{}) error {
	return json.Unmarshal([]byte(e.Message()), v)
}

func (e *event) Acknowledge() error {
	if e.cleanup != nil {
		return e.cleanup()
	}
	return nil
}

func (e *event) ExtendVisibility(duration time.Duration) error {
	if e.extend != nil {
		return e.extend(duration)
	}
	return nil
}

func (e *event) DelayedRetry() error {
	d := e.DeliveryCount() * random(1, 3)
	return e.ExtendVisibility((time.Duration)(d) * time.Minute)
}

func (e *event) SentTimestamp() time.Time {
	return e.sent_ts
}

func (e *event) DeliveryCount() int {
	return e.recv_cnt
}

func random(min, max int) int {
	return rand.Intn(max-min) + min
}
