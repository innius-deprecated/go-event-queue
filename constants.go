package event_queue

import "time"

// Maximum SQS message payload size
const max_message_size = 256000 // 256 KB

// Maximum visibility timeout
const maximum_visibility_timeout = 12 * time.Hour

// minimum allowed visibility timeout
const minimum_visibility_timeout = 3 * time.Second

// Number of seconds to block
const default_number_of_seconds_to_block = 20
const max_number_of_seconds_to_block = 20
const min_number_of_seconds_to_block = 0

// Batch size
const min_batch_size = 1
const max_batch_size = 10

// maximum post batch size: http://docs.aws.amazon.com/cli/latest/reference/sqs/send-message-batch.html
const maximum_post_batch_size_bytes = 262144
const maximum_post_batch_size_count = 10
