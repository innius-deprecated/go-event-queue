package event_queue

import (
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/pkg/errors"
	"context"
	)

type DomainEventQueue interface {
	// Publish an event
	PublishEvent(DomainEvent) error
	PublishEventWithContext(context.Context, DomainEvent) error
	// Publish a batch of events
	PublishEvents([]DomainEvent) error
	// Read a single event, blocking for a standard amount of time.
	ReadEvent() (AckableDomainEvent, error)
	// Read a number of events, blocking for a standard amount of time.
	ReadEvents(number_of_events_max int) ([]AckableDomainEvent, error)
	// Read an event from the queue, providing a duration on how long to wait for a message.
	// The maximum is 20, 0 will cause the call to return straight away. Anything > 1 will poll.
	ReadEventBlocking(seconds_to_block int) (AckableDomainEvent, error)
	// Read a number of events from the queue, providing a duration on how long to wait for a message.
	// The maximum is 20, 0 will cause the call to return straight away. Anything > 1 will poll.
	ReadEventsBlocking(seconds_to_block int, number_of_events_max int) ([]AckableDomainEvent, error)
	// Delete an event. This is done automatically when calling 'Acknowledge' on the AckableDomainEvent
	DeleteEvent(event AckableDomainEvent) error
	// Delete a list of events. This is done automatically when calling 'Acknowledge' on the whole batch of AckableDomainEvents
	//DeleteEvents(events []AckableDomainEvent) error
	// Extend the visibility (the time you can consume this message exclusively).
	ExtendVisibilityTimeout(event AckableDomainEvent, duration time.Duration) error
}

type domain_event_queue struct {
	session           *session.Session
	sqs               *sqs.SQS
	shouldCreateQueue bool
	visibilityTimeout int64
	queueName         string
	queueUrl          string
}

func (deq *domain_event_queue) initialize() error {
	in := &sqs.CreateQueueInput{
		QueueName: &deq.queueName,
	}

	if deq.shouldCreateQueue {
		res, err := deq.sqs.CreateQueue(in)
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("Can't create queue %v", deq.queueName))
		}
		deq.queueUrl = *res.QueueUrl
		return nil
	}

	return nil
}

func (deq *domain_event_queue) PublishEvent(event DomainEvent) error {
	if event == nil {
		return errors.New("Can't publish an event that is nil")
	}
	in := &sqs.SendMessageInput{
		QueueUrl:    &deq.queueUrl,
		MessageBody: aws.String(event.Message()),
	}
	_, err := deq.sqs.SendMessage(in)
	if err != nil {
		return errors.Wrap(err, "SendMessage() failed.")
	}
	return nil
}

func (deq *domain_event_queue) PublishEventWithContext(c context.Context, event DomainEvent) error {
	if event == nil {
		return errors.New("Can't publish an event that is nil")
	}
	in := &sqs.SendMessageInput{
		QueueUrl:    &deq.queueUrl,
		MessageBody: aws.String(event.Message()),
	}
	_, err := deq.sqs.SendMessageWithContext(c, in)
	if err != nil {
		return errors.Wrap(err, "SendMessage() failed.")
	}
	return nil
}

func (deq *domain_event_queue) PublishEvents(events []DomainEvent) error {
	if len(events) == 0 {
		return errors.New("Can't publish an empty event list.")
	}
	evts := make([]*sqs.SendMessageBatchRequestEntry, len(events))

	for i, evt := range events {
		if evt == nil {
			return errors.New("Can't publish an event that is nil")
		}
		evts[i] = &sqs.SendMessageBatchRequestEntry{
			Id:          aws.String(evt.ID()),
			MessageBody: aws.String(evt.Message()),
		}
	}

	// We now have a slice of events to publish. Partition them in separate batches conforming max size rules
	partitions := partition_publish_batch(evts)

	for pcnt, p := range partitions {
		in := &sqs.SendMessageBatchInput{
			QueueUrl: &deq.queueUrl,
			Entries:  p,
		}

		out, err := deq.sqs.SendMessageBatch(in)
		if err != nil {
			return errors.Wrap(err, "SendMessageBatch() failed.")
		}
		if len(out.Failed) > 0 {
			return errors.New(fmt.Sprintf("%d of the %d events failed (in batch %d).", len(out.Failed), len(evts), pcnt))
		}
	}

	return nil
}

func (deq *domain_event_queue) ReadEvent() (AckableDomainEvent, error) {
	return deq.ReadEventBlocking(default_number_of_seconds_to_block)
}

func (deq *domain_event_queue) ReadEventBlocking(seconds_to_block int) (AckableDomainEvent, error) {
	evts, err := deq.ReadEventsBlocking(seconds_to_block, 1)
	if err != nil {
		return nil, err
	}
	if len(evts) == 0 {
		return nil, nil
	}
	return evts[0], nil
}

func (deq *domain_event_queue) ReadEvents(number_of_events_max int) ([]AckableDomainEvent, error) {
	return deq.ReadEventsBlocking(default_number_of_seconds_to_block, number_of_events_max)
}

func (deq *domain_event_queue) ReadEventsBlocking(seconds_to_block int, number_of_events_max int) ([]AckableDomainEvent, error) {
	if seconds_to_block > max_number_of_seconds_to_block || seconds_to_block < min_number_of_seconds_to_block {
		seconds_to_block = default_number_of_seconds_to_block
	}
	if number_of_events_max > max_batch_size || number_of_events_max < min_batch_size {
		number_of_events_max = max_batch_size
	}
	in := &sqs.ReceiveMessageInput{
		AttributeNames: []*string{
			aws.String("SentTimestamp"),
			aws.String("ApproximateReceiveCount"),
		},
		MaxNumberOfMessages: aws.Int64(int64(number_of_events_max)),
		QueueUrl:            &deq.queueUrl,
		VisibilityTimeout:   &deq.visibilityTimeout,
		WaitTimeSeconds:     aws.Int64(int64(seconds_to_block)),
	}
	out, err := deq.sqs.ReceiveMessage(in)
	if err != nil {
		return nil, errors.Wrap(err, "ReceiveMessage() failed.")
	}
	msgs := make([]AckableDomainEvent, len(out.Messages))
	for i, msg := range out.Messages {
		msgs[i] = newEvent(deq, msg)
	}
	return msgs, nil
}

func (deq *domain_event_queue) DeleteEvent(event AckableDomainEvent) error {
	if event == nil {
		return errors.New("Can't delete an event that is nil")
	}
	in := &sqs.DeleteMessageInput{
		QueueUrl:      &deq.queueUrl,
		ReceiptHandle: aws.String(event.Receipt()),
	}
	_, err := deq.sqs.DeleteMessage(in)
	if err != nil {
		return errors.Wrap(err, "DeleteMessage() failed.")
	}
	return nil
}

func (deq *domain_event_queue) ExtendVisibilityTimeout(event AckableDomainEvent, duration time.Duration) error {
	if event == nil {
		return errors.New("Can't extend an event that is nil")
	}
	if duration.Seconds() < minimum_visibility_timeout.Seconds() {
		return errors.New(fmt.Sprintf("The minimum allowed visibility duration is %v, provided with %v", minimum_visibility_timeout, duration))
	}
	if duration.Seconds() > maximum_visibility_timeout.Seconds() {
		return errors.New(fmt.Sprintf("The maximum allowed visibility duration is %v, provided with %v", maximum_visibility_timeout, duration))
	}
	in := &sqs.ChangeMessageVisibilityInput{
		QueueUrl:          &deq.queueUrl,
		ReceiptHandle:     aws.String(event.Receipt()),
		VisibilityTimeout: aws.Int64(int64(duration.Seconds())),
	}
	_, err := deq.sqs.ChangeMessageVisibility(in)
	if err != nil {
		return errors.Wrap(err, "ExtendVisibilityTimeout() failed.")
	}
	return nil
}

func partition_publish_batch(evts []*sqs.SendMessageBatchRequestEntry) [][]*sqs.SendMessageBatchRequestEntry {
	size := 0
	res := [][]*sqs.SendMessageBatchRequestEntry{}
	partition := []*sqs.SendMessageBatchRequestEntry{}
	for _, evt := range evts {
		exceeds_max_message_bytes := (size + len(*evt.MessageBody)) > maximum_post_batch_size_bytes
		exceeds_max_message_cnt := (len(partition) + 1) > maximum_post_batch_size_count

		if exceeds_max_message_bytes || exceeds_max_message_cnt {
			res = append(res, partition)
			partition = []*sqs.SendMessageBatchRequestEntry{}
			size = 0
		}

		partition = append(partition, evt)
		size = size + len(*evt.MessageBody)
	}
	res = append(res, partition)
	return res
}
