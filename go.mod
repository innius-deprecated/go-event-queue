module bitbucket.org/to-increase/go-event-queue

require (
	bitbucket.org/to-increase/go-trn v1.0.1
	github.com/aws/aws-sdk-go v1.19.21
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.0
	github.com/stretchr/testify v1.3.0
	golang.org/x/net v0.0.0-20190327214358-63eda1eb0650 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
