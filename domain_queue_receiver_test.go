package event_queue_test

import (
	"context"
	"bitbucket.org/to-increase/go-event-queue"
	_ "encoding/json"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	_ "reflect"
	"testing"
"encoding/json"
)

type testHandler struct {
	called     bool
	shouldFail bool
}

func (h *testHandler) HandleEvent(c context.Context, evt *event_queue.RawEvent) error {
	h.called = true
	if h.shouldFail {
		return errors.New("failed")
	} else {
		return nil
	}
}

type testHooks struct {
	beforeHandleCalled     bool
	beforeHandleShouldFail bool
	onErrorCalled          bool
	onCompletionCalled     bool
}

func (h *testHooks) BeforeHandleEvent(c context.Context, m event_queue.RawEvent) error {
	h.beforeHandleCalled = true
	if h.beforeHandleShouldFail {
		return errors.New("failed")
	} else {
		return nil
	}
}

func (h *testHooks) OnError(c context.Context, m event_queue.RawEvent, e event_queue.AckableDomainEvent) {
	h.onErrorCalled = true
}

func (h *testHooks) OnCompletion(c context.Context, m event_queue.RawEvent) error {
	h.onCompletionCalled = true
	return nil
}

func createTestReceiver(t *testing.T) (event_queue.Receiver, *testHandler, *testHooks) {
	handler := &testHandler{}
	hooks := &testHooks{}
	return event_queue.NewDomainQueueReceiver([]event_queue.EventHandler{handler}, 1, 1, hooks), handler, hooks
}

type TestEvent struct {
	Message string
}

func insertTestData(t *testing.T) event_queue.DomainEventQueue {
	deq := initDeqWithQueue(t, "receiver-queue")
	e := TestEvent{Message: "this is a test"}
	msg := struct {
		TestEvent `json:"Event"`
		LogLevel  string `json:"loglevel"`
		RequestID string `json:"requestid"`
		Token     string `json:"token"`
		ID        string `json:"id"`
	}{
		e, "loglevel", "requestid", "token", "id",
	}
	evt, err := event_queue.NewEventJson(msg)
	err = deq.PublishEvent(evt)
	assert.Nil(t, err)
	return deq
}

func TestHappyFlow(t *testing.T) {
	receiver, handler, hooks := createTestReceiver(t)
	deq := insertTestData(t)
	receiver.ReceiveOnce(context.Background(), deq)
	assert.True(t, hooks.beforeHandleCalled)
	assert.True(t, handler.called)
	assert.True(t, hooks.onCompletionCalled)
}

func TestBeforeFailed(t *testing.T) {
	receiver, handler, hooks := createTestReceiver(t)
	hooks.beforeHandleShouldFail = true
	deq := insertTestData(t)
	receiver.ReceiveOnce(context.Background(), deq)
	assert.True(t, hooks.beforeHandleCalled)
	assert.False(t, handler.called)
}

func TestHandleFailed(t *testing.T) {
	receiver, handler, hooks := createTestReceiver(t)
	handler.shouldFail = true
	deq := insertTestData(t)
	receiver.ReceiveOnce(context.Background(), deq)
	assert.True(t, hooks.beforeHandleCalled)
	assert.True(t, handler.called)
	assert.True(t, hooks.onErrorCalled)
	assert.False(t, hooks.onCompletionCalled)
}

func TestEventParsing(t *testing.T) {
	r := `{
		"type": "testType",
		"data": {}
	}`
	evt := &event_queue.RawEvent{}
	err := json.Unmarshal([]byte(r), evt)
	assert.Nil(t, err)
	if err != nil {
		println(err.Error())
	}
	assert.Equal(t, "testType", evt.Type)
}
