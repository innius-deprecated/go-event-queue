package event_queue_test

import (
	"bitbucket.org/to-increase/go-event-queue"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestPublishReadAcknowledgementRoundtrip(t *testing.T) {
	deq := initDeqWithQueue(t, fmt.Sprintf("go-event-queue-r-%v", time.Now().Unix()))
	type foo struct {
		Foo string `json:"foo"`
		Bar int    `json:"bar"`
	}
	payload := &foo{
		Foo: "foo",
		Bar: 42,
	}
	evt, err := event_queue.NewEventJson(payload)
	assert.Nil(t, err)
	err = deq.PublishEvent(evt)
	assert.Nil(t, err)

	// Retrieve it.
	read_evt, err := deq.ReadEvent()
	assert.Nil(t, err)

	// Verify its content
	m := &foo{}
	err = read_evt.Unmarshal(m)
	assert.Nil(t, err)

	assert.Equal(t, "foo", m.Foo)
	assert.Equal(t, 42, m.Bar)

	// Acknowledge the reception
	err = read_evt.Acknowledge()
	assert.Nil(t, err)

	// Retrieving the message again should not be possible now.
	read_evt_two, err := deq.ReadEventBlocking(0)
	assert.Nil(t, err)
	assert.Nil(t, read_evt_two)
}

func TestPublishReadAcknowledgementRoundtripMixed(t *testing.T) {
	timeout := time.Second * 3
	// Create
	s := stubSession()
	builder := event_queue.NewDomainEventQueueBuilder()
	deq, err := builder.
		WithSession(s).
		AutoCreateQueue(true).
		WithVisibilityTimeout(timeout).
		WithQueueName(fmt.Sprintf("go-event-queue-m-%v", time.Now().Unix())).
		Build()

	assert.NotNil(t, deq)
	assert.Nil(t, err)

	size := 10
	evts := make([]event_queue.DomainEvent, size)
	// Start counting at 1
	for i := 0; i < size; i++ {
		evts[i] = event_queue.NewEventStringUnsafe(fmt.Sprintf("abc-%d", i+1))
	}

	seen := make(map[string]bool)

	// Publish a single event, then a batch, then 2 more singles
	err = deq.PublishEvent(evts[0])
	assert.Nil(t, err)
	err = deq.PublishEvents(evts[0:8])
	assert.Nil(t, err)
	err = deq.PublishEvent(evts[8])
	assert.Nil(t, err)
	err = deq.PublishEvent(evts[9])
	assert.Nil(t, err)

	// Retrieve 2 single events
	a, err := deq.ReadEvent()
	assert.Nil(t, err)
	seen[a.Message()] = true
	b, err := deq.ReadEvent()
	assert.Nil(t, err)
	seen[b.Message()] = true

	// Retrieve a batch of 3 events
	evts_x, err := deq.ReadEvents(3)
	assert.Nil(t, err)
	assert.Len(t, evts_x, 3)
	seen[evts_x[0].Message()] = true
	seen[evts_x[1].Message()] = true
	seen[evts_x[2].Message()] = true

	c, err := deq.ReadEvent()
	assert.Nil(t, err)
	seen[c.Message()] = true

	// Acknowledge / delete all but one message.
	assert.Nil(t, a.Acknowledge())
	assert.Nil(t, b.Acknowledge())
	assert.Nil(t, evts_x[0].Acknowledge())
	assert.Nil(t, evts_x[1].Acknowledge())
	assert.Nil(t, evts_x[2].Acknowledge())
	assert.Nil(t, c.Acknowledge())
}
