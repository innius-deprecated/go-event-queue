package event_queue

import (
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type RawEvent struct {
	Type string `json:"type"`
	Data json.RawMessage
}

type EventHandler interface {
	HandleEvent(context.Context, *RawEvent) error
}

type Message struct {
	Event     RawEvent
	Token     string `json:"token"`
	RequestID string `json:"requestid"`
	LogLevel  string `json:"loglevel"`
	ID        string `json:"id"`
}

// return a new queue receiver instance
func NewDomainQueueReceiver(hs []EventHandler, stb, noem int, hooks ReceiveHooks) Receiver {
	return &receiver{
		handlers:          hs,
		secondsToBlock:    stb,
		numberOfEventsMax: noem,
		receiveHooks:      hooks,
	}
}

type Receiver interface {
	Receive(context.Context, DomainEventQueue)
	ReceiveOnce(context.Context, DomainEventQueue)
}

type receiver struct {
	handlers          []EventHandler
	secondsToBlock    int
	numberOfEventsMax int
	receiveHooks      ReceiveHooks
}

func (r *receiver) Receive(c context.Context, queue DomainEventQueue) {
	logrus.Debug("queue receiver has been started")
	for {
		r.ReceiveOnce(c, queue)
	}
}

func (r *receiver) ReceiveOnce(c context.Context, queue DomainEventQueue) {
	evts, err := queue.ReadEventsBlocking(r.secondsToBlock, r.numberOfEventsMax)
	if len(evts) > 0 {
		logrus.Debugf("received %d messages from queue", len(evts))
	}
	if err != nil {
		logrus.Errorf("%+v", err)
	}
	for i := range evts {
		var msg RawEvent
		if err := evts[i].Unmarshal(&msg); err != nil {
			logrus.Error(errors.Wrap(err, "could not unmarshal event message"))
			continue
		}
		err := r.receiveHooks.BeforeHandleEvent(c, msg)
		if err != nil {
			logrus.Error(errors.Wrap(err, "BeforeHandleEvent failed"))
			continue
		}
		for _, h := range r.handlers {
			err := h.HandleEvent(c, &msg)
			if err != nil {
				logrus.Errorf("Handle event %v failed: %v", msg, err)
				r.receiveHooks.OnError(c, msg, evts[i])
			} else {
				evts[i].Acknowledge()
				if err := r.receiveHooks.OnCompletion(c, msg); err != nil {
					logrus.Errorf("EventCompleted error: %v", err)
				}
			}
		}
	}
}
