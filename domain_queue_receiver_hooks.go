package event_queue

import (
	"context"
	"github.com/sirupsen/logrus"
)

type ReceiveHooks interface {
	BeforeHandleEvent(context.Context, RawEvent) error
	OnError(context.Context, RawEvent, AckableDomainEvent)
	OnCompletion(context.Context, RawEvent) error
}

type DefaultHooks struct{}

func (h *DefaultHooks) BeforeHandleEvent(c context.Context, m RawEvent) error { return nil }

func (h *DefaultHooks) OnError(c context.Context, m RawEvent, e AckableDomainEvent) {
	if err := e.DelayedRetry(); err != nil {
		logrus.Errorf("DelayedRetry error: %v", err)
	}
}

func (h *DefaultHooks) OnCompletion(c context.Context, m RawEvent) error { return nil }
